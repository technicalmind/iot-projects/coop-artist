import RPi.GPIO as GPIO
from flask import Flask, jsonify

# Init
app = Flask( __name__)

# Create dictionary with elements 
things = { 'door': [ { 'name': 'coop', 'gpio': { 'open': [ 23, 22], 'close': [ 22, 23]}}]}

# Initialize pins
GPIO.setmode( GPIO.BCM)
GPIO.setup( 22, GPIO.OUT)
GPIO.setup( 23, GPIO.OUT)


def log( line):
    print( line)


def thing_do( thing, command):
    result = 400
    log( "function: thing_do - Command: " + command)
    thing_gpio = thing[ 'gpio']
    if command in thing_gpio:
        pin_list = thing_gpio[ command]
        GPIO.output( pin_list[ 0], GPIO.HIGH)
        GPIO.output( pin_list[ 1], GPIO.LOW)
        result = 200
    return result


@app.route( "/status")
def status():
    return 'online', 200


@app.route( "/<thing_type>/<thing_name>/<action_command>")
def action( thing_type, thing_name, action_command):
    result = 400
    if thing_type in things:
        for thing_entry in things[ thing_type]:
            if thing_name == thing_entry[ 'name'] or thing_name == "all":
                result = thing_do(thing_entry, action_command)
    return jsonify( { 'res': result}), result


if __name__ == "__main__":
    app.run( host = '0.0.0.0', port = 80, debug = False)
